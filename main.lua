utf8 = require 'utf8'

require 'export'

-- run before each test and also before a real run
function love.load(args)
  Header_font = initialize_font(30)
  Body_font = initialize_font(20)
  Log = {}
  Margin_top = 25
  Margin_left = 25
  Drawing_text_baseline_height = love.graphics.newFont(20):getBaseline()  -- keep sync'd with Font_height in lines.love

  File_select_bar_value = ''

  Header = {
    'Either type in a file name above',
    '(from under the love app dir),',
    'or drag a file (from anywhere)',
    'on to this window',
    'to convert it to html+inline SVG images.'
  }

  --
  Cursor_time = 0

  love.keyboard.setTextInput(true)  -- bring up keyboard on touch screen
  love.keyboard.setKeyRepeat(true)

  love.window.setTitle('lines.love export to markdown and SVG')

  love.graphics.setBackgroundColor(1,1,1)

  local flags
  _, _, flags = love.window.getMode()
  flags.resizable = true
  love.window.setMode(800, 800, flags)

  for _,arg in ipairs(args) do
    export(arg)
  end
end

function initialize_font(font_height)
  return {data=love.graphics.newFont(font_height), font_height=font_height, line_height=font_height*1.3}
end

function love.filedropped(file)
  export(file:getFilename())
end

function log_event(line)
  table.insert(Log, line)
  while #Log > 10 do
    table.remove(Log, 1)
  end
end

function love.draw()
  love.graphics.setColor(0,0,0)

  draw_file_select_bar()

  love.graphics.setFont(Header_font.data)
  local y = Margin_top + 4 * Body_font.line_height
  for _,line in ipairs(Header) do
    love.graphics.print(line, Margin_left, y)
    y = y + Header_font.line_height
  end

  y = y + 2*Header_font.line_height
  love.graphics.setFont(Body_font.data)
  for i=#Log,1,-1 do
    love.graphics.print(Log[i], Margin_left, y)
    y = y + Body_font.line_height
  end
end

function draw_file_select_bar()
  local h = Body_font.line_height+2
  local y = 10
  love.graphics.setColor(0.9,0.9,0.9)
  love.graphics.rectangle('fill', 0, y-10, 800-1, h+8)
  love.graphics.setColor(0.6,0.6,0.6)
  love.graphics.line(0, y-10, 800-1, y-10)
  love.graphics.setColor(0,0,0)
  local prompt = 'open file: '
  love.graphics.print(prompt, 15, y-5)
  love.graphics.setColor(1,1,1)
  local text_start_x = width(prompt) + 20
  love.graphics.rectangle('fill', text_start_x-5, y-6, 800-text_start_x-20, h+2, 2,2)
  love.graphics.setColor(0.6,0.6,0.6)
  love.graphics.rectangle('line', text_start_x-5, y-6, 800-text_start_x-20, h+2, 2,2)
  love.graphics.setColor(0,0,0)
  love.graphics.print(File_select_bar_value, text_start_x,y-5)
  draw_cursor(text_start_x+width(File_select_bar_value), y-5)
end

function draw_cursor(x, y)
  -- blink every 0.5s
  if math.floor(Cursor_time*2)%2 == 0 then
    love.graphics.setColor(1,0,0)
    love.graphics.rectangle('fill', x,y, 3,Body_font.line_height)
    love.graphics.setColor(0,0,0)
  end
end

function love.update(dt)
  Cursor_time = Cursor_time + dt
end

function love.keypressed(key, scancode)
  Cursor_time = 0  -- ensure cursor is visible immediately after it moves
  if key == 'escape' then
    File_select_bar_value = ''
  elseif key == 'return' then
    export(love.filesystem.getSourceBaseDirectory()..'/'..File_select_bar_value)
    File_select_bar_value = ''
  elseif key == 'backspace' then
    local len = utf8.len(File_select_bar_value)
    local byte_offset = safe_utf8_offset(File_select_bar_value, len)
    File_select_bar_value = string.sub(File_select_bar_value, 1, byte_offset-1)
  end
end

function love.textinput(t)
  Cursor_time = 0  -- ensure cursor is visible immediately after it moves
  File_select_bar_value = File_select_bar_value..t
end

function safe_utf8_offset(s, pos1)
  if pos1 == 1 then return 1 end
  local result = utf8.offset(s, pos1)
  if result == nil then
    print(pos1, #s, s)
  end
  assert(result)
  return result
end

function width(text)
  return love.graphics.getFont():getWidth(text)
end
