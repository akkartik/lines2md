# export plain text files containing line drawings to markdown

This repository is a supporting app to [lines.love](http://akkartik.name/lines.html),
an editor for plain text where you can also seamlessly insert line drawings.
It allows you to export your plain text containing line drawings to markdown
files linking to SVG images.

Here's a video showing it in action:

![demo](lines-export.gif)

## Mirrors and Forks

Updates to this repo can be downloaded from:

* https://codeberg.org/akkartik/lines2md

Forks of this repo are encouraged. If you show me your fork, I'll link to it
here.

* https://git.sr.ht/~akkartik/lines2html.love
