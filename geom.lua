local geom = {}

function geom.rotate(cx,cy, x,y, theta)
  local x2,y2 = x-cx,y-cy
  local x3 = x2*math.cos(theta) - y2*math.sin(theta)
  local y3 = x2*math.sin(theta) + y2*math.cos(theta)
  return cx+x3,cy+y3
end

-- result is from -π/2 to 3π/2, approximately adding math.atan2 from Lua 5.3
-- (LÖVE is Lua 5.1)
function geom.angle(x1,y1, x2,y2)
  local result = math.atan((y2-y1)/(x2-x1))
  if x2 < x1 then
    result = result+math.pi
  end
  return result
end

function geom.dist(x1,y1, x2,y2) return ((x2-x1)^2+(y2-y1)^2)^0.5 end

return geom
